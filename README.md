# uic for ngwidget

使用方法与uic一致，具体可以查看帮助。默认分支“main”仅支持qt6及以上版本，qt5版本请使用分支“qt5”。

```bash
nguic --help
```

```
Usage: D:\workbench\sources\projects\nguic\build\source\nguic.exe [options] [uifile]
NG User Interface Compiler version 0.0.1

Options:
  -?, -h, --help                  Displays help on commandline options.
  --help-all                      Displays help including Qt specific options.
  -v, --version                   Displays version information.
  -d, --dependencies              Display the dependencies.
  -o, --output <file>             Place the output into <file>
  -a, --no-autoconnection         Do not generate a call to
                                  QObject::connectSlotsByName().
  -p, --no-protection             Disable header protection.
  -n, --no-implicit-includes      Disable generation of #include-directives.
  --postfix <postfix>             Postfix to add to all generated classnames.
  --tr, --translate <function>    Use <function> for i18n.
  --include <include-file>        Add #include <include-file> to <file>.
  -g, --generator <python|cpp>    Select generator.
  -c, --connections <pmf|string>  Connection syntax.
  -s, --namespace <namespace>     Use custom <namespace>.
  --idbased                       Use id based function for i18n
  --from-imports                  Python: generate imports relative to '.'
  --absolute-imports              Python: generate absolute imports
  --rc-prefix                     Python: Generate "rc_file" instead of
                                  "file_rc" import
  --star-imports                  Python: Use * imports
  --python-paths <pathlist>       Python paths for --absolute-imports.

Arguments:
  [uifile]                        Input file (*.ui), otherwise stdin
```

常见用法，将ui文件生成为头文件：
```bash
nguic test.ui -o testui.h
```

增加自定义命名空间支持：
```bash
# 指定命名空间为nggesture
nguic -s nggesture test.ui -o ui_test.h
```
生成的代码如下（部分代码已经省去）：
```c++
/********************************************************************************
** Form generated from reading UI file 'nggesturewindow.ui'
**
** Created by: NG User Interface Compiler version 0.0.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef NGGESTUREWINDOWUI_H
#define NGGESTUREWINDOWUI_H

...
#include "ngwidgets/ngmainwindow.h"

namespace nggesture
{

class Ui_NGGestureWindow
{
public:
    ...

    void setupUi(ngui::NGMainWindow *NGGestureWindow)
    {
        ...
    } // setupUi

    void retranslateUi(ngui::NGMainWindow *NGGestureWindow)
    {
        ...
    } // retranslateUi

};

namespace Ui {
    class NGGestureWindow: public Ui_NGGestureWindow {};
} // namespace Ui

}  // namespace nggesture

#endif // NGGESTUREWINDOWUI_H

```


## 测试

执行下面命令完成构建和测试用例执行：

```bash
cmake -B build -S . && cmake --build build && ctest --test-dir build
```

该命令会将tests/test_widget.ui解析生成tests/test_widget.h。

![nguic](./asserts/nguic.gif)
