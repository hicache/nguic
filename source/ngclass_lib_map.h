// Qt类与NGWidgets类的映射关系
// Qt类名，NGWidgets类（带命名空间），NGWidgets类的头文件路径
NG_CLASS_LIB(QColorDialog, ngui::NGColorDialog, ngwidgets/ngcolordialog.h)
NG_CLASS_LIB(QDialog, ngui::NGDialog, ngwidgets/ngdialog.h)
NG_CLASS_LIB(QFileDialog, ngui::NGFileDialog, ngwidgets/ngfiledialog.h)
NG_CLASS_LIB(QFileSystemModel, ngui::NGFileSystemModel, ngwidgets/ngfilesystemmodel.h)
NG_CLASS_LIB(QFontDialog, ngui::NGFontDialog, ngwidgets/ngfontdialog.h)
NG_CLASS_LIB(QInputDialog, ngui::NGInputDialog, ngwidgets/nginputdialog.h)
NG_CLASS_LIB(QMessageBox, ngui::NGMessageBox, ngwidgets/ngmessagebox.h)
NG_CLASS_LIB(QProgressDialog, ngui::NGProgressDialog, ngwidgets/ngprogressdialog.h)
NG_CLASS_LIB(QWizard, ngui::NGWizard, ngwidgets/ngwizard.h)
NG_CLASS_LIB(QWizardPage, ngui::NGWizardPage, ngwidgets/ngwizardpage.h)
NG_CLASS_LIB(QGraphicsEffect, ngui::NGGraphicsEffect, ngwidgets/nggraphicseffect.h)
NG_CLASS_LIB(QGraphicsColorizeEffect, ngui::NGGraphicsColorizeEffect, ngwidgets/nggraphicscolorizeeffect.h)
NG_CLASS_LIB(QGraphicsBlurEffect, ngui::NGGraphicsBlurEffect, ngwidgets/nggraphicsblureffect.h)
NG_CLASS_LIB(QGraphicsDropShadowEffect, ngui::NGGraphicsDropShadowEffect, ngwidgets/nggraphicsdropshadoweffect.h)
NG_CLASS_LIB(QGraphicsOpacityEffect, ngui::NGGraphicsOpacityEffect, ngwidgets/nggraphicsopacityeffect.h)
NG_CLASS_LIB(QGraphicsAnchor, ngui::NGGraphicsAnchor, ngwidgets/nggraphicsanchor.h)
NG_CLASS_LIB(QGraphicsAnchorLayout, ngui::NGGraphicsAnchorLayout, ngwidgets/nggraphicsanchorlayout.h)
NG_CLASS_LIB(QGraphicsGridLayout, ngui::NGGraphicsGridLayout, ngwidgets/nggraphicsgridlayout.h)
NG_CLASS_LIB(QGraphicsItem, ngui::NGGraphicsItem, ngwidgets/nggraphicsitem.h)
NG_CLASS_LIB(QGraphicsObject, ngui::NGGraphicsObject, ngwidgets/nggraphicsobject.h)
NG_CLASS_LIB(QAbstractGraphicsShapeItem, ngui::NGAbstractGraphicsShapeItem, ngwidgets/ngabstractgraphicsshapeitem.h)
NG_CLASS_LIB(QGraphicsPathItem, ngui::NGGraphicsPathItem, ngwidgets/nggraphicspathitem.h)
NG_CLASS_LIB(QGraphicsRectItem, ngui::NGGraphicsRectItem, ngwidgets/nggraphicsrectitem.h)
NG_CLASS_LIB(QGraphicsEllipseItem, ngui::NGGraphicsEllipseItem, ngwidgets/nggraphicsellipseitem.h)
NG_CLASS_LIB(QGraphicsPolygonItem, ngui::NGGraphicsPolygonItem, ngwidgets/nggraphicspolygonitem.h)
NG_CLASS_LIB(QGraphicsLineItem, ngui::NGGraphicsLineItem, ngwidgets/nggraphicslineitem.h)
NG_CLASS_LIB(QGraphicsPixmapItem, ngui::NGGraphicsPixmapItem, ngwidgets/nggraphicspixmapitem.h)
NG_CLASS_LIB(QGraphicsTextItem, ngui::NGGraphicsTextItem, ngwidgets/nggraphicstextitem.h)
NG_CLASS_LIB(QGraphicsSimpleTextItem, ngui::NGGraphicsSimpleTextItem, ngwidgets/nggraphicssimpletextitem.h)
NG_CLASS_LIB(QGraphicsItemGroup, ngui::NGGraphicsItemGroup, ngwidgets/nggraphicsitemgroup.h)
NG_CLASS_LIB(QGraphicsItemAnimation, ngui::NGGraphicsItemAnimation, ngwidgets/nggraphicsitemanimation.h)
NG_CLASS_LIB(QGraphicsLayout, ngui::NGGraphicsLayout, ngwidgets/nggraphicslayout.h)
NG_CLASS_LIB(QGraphicsLayoutItem, ngui::NGGraphicsLayoutItem, ngwidgets/nggraphicslayoutitem.h)
NG_CLASS_LIB(QGraphicsLinearLayout, ngui::NGGraphicsLinearLayout, ngwidgets/nggraphicslinearlayout.h)
NG_CLASS_LIB(QGraphicsProxyWidget, ngui::NGGraphicsProxyWidget, ngwidgets/nggraphicsproxywidget.h)
NG_CLASS_LIB(QGraphicsScene, ngui::NGGraphicsScene, ngwidgets/nggraphicsscene.h)
NG_CLASS_LIB(QGraphicsSceneEvent, ngui::NGGraphicsSceneEvent, ngwidgets/nggraphicssceneevent.h)
NG_CLASS_LIB(QGraphicsSceneMouseEvent, ngui::NGGraphicsSceneMouseEvent, ngwidgets/nggraphicsscenemouseevent.h)
NG_CLASS_LIB(QGraphicsSceneWheelEvent, ngui::NGGraphicsSceneWheelEvent, ngwidgets/nggraphicsscenewheelevent.h)
NG_CLASS_LIB(QGraphicsSceneContextMenuEvent, ngui::NGGraphicsSceneContextMenuEvent, ngwidgets/nggraphicsscenecontextmenuevent.h)
NG_CLASS_LIB(QGraphicsSceneHoverEvent, ngui::NGGraphicsSceneHoverEvent, ngwidgets/nggraphicsscenehoverevent.h)
NG_CLASS_LIB(QGraphicsSceneHelpEvent, ngui::NGGraphicsSceneHelpEvent, ngwidgets/nggraphicsscenehelpevent.h)
NG_CLASS_LIB(QGraphicsSceneDragDropEvent, ngui::NGGraphicsSceneDragDropEvent, ngwidgets/nggraphicsscenedragdropevent.h)
NG_CLASS_LIB(QGraphicsSceneResizeEvent, ngui::NGGraphicsSceneResizeEvent, ngwidgets/nggraphicssceneresizeevent.h)
NG_CLASS_LIB(QGraphicsSceneMoveEvent, ngui::NGGraphicsSceneMoveEvent, ngwidgets/nggraphicsscenemoveevent.h)
NG_CLASS_LIB(QGraphicsTransform, ngui::NGGraphicsTransform, ngwidgets/nggraphicstransform.h)
NG_CLASS_LIB(QGraphicsScale, ngui::NGGraphicsScale, ngwidgets/nggraphicsscale.h)
NG_CLASS_LIB(QGraphicsRotation, ngui::NGGraphicsRotation, ngwidgets/nggraphicsrotation.h)
NG_CLASS_LIB(QGraphicsView, ngui::NGGraphicsView, ngwidgets/nggraphicsview.h)
NG_CLASS_LIB(QGraphicsWidget, ngui::NGGraphicsWidget, ngwidgets/nggraphicswidget.h)
NG_CLASS_LIB(QAbstractItemDelegate, ngui::NGAbstractItemDelegate, ngwidgets/ngabstractitemdelegate.h)
NG_CLASS_LIB(QAbstractItemView, ngui::NGAbstractItemView, ngwidgets/ngabstractitemview.h)
NG_CLASS_LIB(QAbstractProxyModel, ngui::NGAbstractProxyModel, ngwidgets/ngabstractproxymodel.h)
NG_CLASS_LIB(QColumnView, ngui::NGColumnView, ngwidgets/ngcolumnview.h)
NG_CLASS_LIB(QDataWidgetMapper, ngui::NGDataWidgetMapper, ngwidgets/ngdatawidgetmapper.h)
NG_CLASS_LIB(QFileIconProvider, ngui::NGFileIconProvider, ngwidgets/ngfileiconprovider.h)
NG_CLASS_LIB(QHeaderView, ngui::NGHeaderView, ngwidgets/ngheaderview.h)
NG_CLASS_LIB(QItemDelegate, ngui::NGItemDelegate, ngwidgets/ngitemdelegate.h)
NG_CLASS_LIB(QItemEditorCreatorBase, ngui::NGItemEditorCreatorBase, ngwidgets/ngitemeditorcreatorbase.h)
NG_CLASS_LIB(QItemEditorCreator, ngui::NGItemEditorCreator, ngwidgets/ngitemeditorcreator.h)
NG_CLASS_LIB(QStandardItemEditorCreator, ngui::NGStandardItemEditorCreator, ngwidgets/ngstandarditemeditorcreator.h)
NG_CLASS_LIB(QItemEditorFactory, ngui::NGItemEditorFactory, ngwidgets/ngitemeditorfactory.h)
NG_CLASS_LIB(QItemSelectionRange, ngui::NGItemSelectionRange, ngwidgets/ngitemselectionrange.h)
NG_CLASS_LIB(QItemSelectionModel, ngui::NGItemSelectionModel, ngwidgets/ngitemselectionmodel.h)
NG_CLASS_LIB(QItemSelection, ngui::NGItemSelection, ngwidgets/ngitemselection.h)
NG_CLASS_LIB(QListView, ngui::NGListView, ngwidgets/nglistview.h)
NG_CLASS_LIB(QListWidgetItem, ngui::NGListWidgetItem, ngwidgets/nglistwidgetitem.h)
NG_CLASS_LIB(QListWidget, ngui::NGListWidget, ngwidgets/nglistwidget.h)
NG_CLASS_LIB(QStyledItemDelegate, ngui::NGStyledItemDelegate, ngwidgets/ngstyleditemdelegate.h)
NG_CLASS_LIB(QTableView, ngui::NGTableView, ngwidgets/ngtableview.h)
NG_CLASS_LIB(QTableWidgetSelectionRange, ngui::NGTableWidgetSelectionRange, ngwidgets/ngtablewidgetselectionrange.h)
NG_CLASS_LIB(QTableWidgetItem, ngui::NGTableWidgetItem, ngwidgets/ngtablewidgetitem.h)
NG_CLASS_LIB(QTableWidget, ngui::NGTableWidget, ngwidgets/ngtablewidget.h)
NG_CLASS_LIB(QTreeView, ngui::NGTreeView, ngwidgets/ngtreeview.h)
NG_CLASS_LIB(QTreeWidgetItem, ngui::NGTreeWidgetItem, ngwidgets/ngtreewidgetitem.h)
NG_CLASS_LIB(QTreeWidget, ngui::NGTreeWidget, ngwidgets/ngtreewidget.h)
NG_CLASS_LIB(QTreeWidgetItemIterator, ngui::NGTreeWidgetItemIterator, ngwidgets/ngtreewidgetitemiterator.h)
// NG_CLASS_LIB(QApplication, ngui::NGApplication, ngwidgets/ngapplication.h)
NG_CLASS_LIB(QCursorShape, ngui::NGCursorShape, ngwidgets/ngcursorshape.h)
NG_CLASS_LIB(QDrag, ngui::NGDrag, ngwidgets/ngdrag.h)
NG_CLASS_LIB(QGesture, ngui::NGGesture, ngwidgets/nggesture.h)
NG_CLASS_LIB(QPanGesture, ngui::NGPanGesture, ngwidgets/ngpangesture.h)
NG_CLASS_LIB(QPinchGesture, ngui::NGPinchGesture, ngwidgets/ngpinchgesture.h)
NG_CLASS_LIB(QSwipeGesture, ngui::NGSwipeGesture, ngwidgets/ngswipegesture.h)
NG_CLASS_LIB(QTapGesture, ngui::NGTapGesture, ngwidgets/ngtapgesture.h)
NG_CLASS_LIB(QTapAndHoldGesture, ngui::NGTapAndHoldGesture, ngwidgets/ngtapandholdgesture.h)
NG_CLASS_LIB(QGestureRecognizer, ngui::NGGestureRecognizer, ngwidgets/nggesturerecognizer.h)
NG_CLASS_LIB(QSpacerItem, ngui::NGSpacerItem, ngwidgets/ngspaceritem.h)
NG_CLASS_LIB(QWidgetItem, ngui::NGWidgetItem, ngwidgets/ngwidgetitem.h)
NG_CLASS_LIB(QWidgetItemV2, ngui::NGWidgetItemV2, ngwidgets/ngwidgetitemv2.h)
NG_CLASS_LIB(QColorGroup, ngui::NGColorGroup, ngwidgets/ngcolorgroup.h)
NG_CLASS_LIB(QSizePolicy, ngui::NGSizePolicy, ngwidgets/ngsizepolicy.h)
NG_CLASS_LIB(QToolTip, ngui::NGToolTip, ngwidgets/ngtooltip.h)
NG_CLASS_LIB(QWhatsThis, ngui::NGWhatsThis, ngwidgets/ngwhatsthis.h)
NG_CLASS_LIB(QWidgetData, ngui::NGWidgetData, ngwidgets/ngwidgetdata.h)
NG_CLASS_LIB(QWidget, ngui::NGWidget, ngwidgets/ngwidget.h)
NG_CLASS_LIB(QAction, ngui::NGAction, ngwidgets/ngaction.h)
NG_CLASS_LIB(QWidgetAction, ngui::NGWidgetAction, ngwidgets/ngwidgetaction.h)
NG_CLASS_LIB(QWidgetList, ngui::NGWidgetList, ngwidgets/ngwidgetlist.h)
NG_CLASS_LIB(QWidgetMapper, ngui::NGWidgetMapper, ngwidgets/ngwidgetmapper.h)
NG_CLASS_LIB(QWidgetSet, ngui::NGWidgetSet, ngwidgets/ngwidgetset.h)
NG_CLASS_LIB(QWindowSystemInterface, ngui::NGWindowSystemInterface, ngwidgets/ngwindowsysteminterface.h)
NG_CLASS_LIB(QCommonStyle, ngui::NGCommonStyle, ngwidgets/ngcommonstyle.h)
NG_CLASS_LIB(QProxyStyle, ngui::NGProxyStyle, ngwidgets/ngproxystyle.h)
NG_CLASS_LIB(QStyle, ngui::NGStyle, ngwidgets/ngstyle.h)
NG_CLASS_LIB(QStyleFactory, ngui::NGStyleFactory, ngwidgets/ngstylefactory.h)
NG_CLASS_LIB(QStyleOption, ngui::NGStyleOption, ngwidgets/ngstyleoption.h)
NG_CLASS_LIB(QStyleOptionFocusRect, ngui::NGStyleOptionFocusRect, ngwidgets/ngstyleoptionfocusrect.h)
NG_CLASS_LIB(QStyleOptionFrame, ngui::NGStyleOptionFrame, ngwidgets/ngstyleoptionframe.h)
NG_CLASS_LIB(QStyleOptionTabWidgetFrame, ngui::NGStyleOptionTabWidgetFrame, ngwidgets/ngstyleoptiontabwidgetframe.h)
NG_CLASS_LIB(QStyleOptionTabBarBase, ngui::NGStyleOptionTabBarBase, ngwidgets/ngstyleoptiontabbarbase.h)
NG_CLASS_LIB(QStyleOptionHeader, ngui::NGStyleOptionHeader, ngwidgets/ngstyleoptionheader.h)
NG_CLASS_LIB(QStyleOptionButton, ngui::NGStyleOptionButton, ngwidgets/ngstyleoptionbutton.h)
NG_CLASS_LIB(QStyleOptionTab, ngui::NGStyleOptionTab, ngwidgets/ngstyleoptiontab.h)
NG_CLASS_LIB(QStyleOptionToolBar, ngui::NGStyleOptionToolBar, ngwidgets/ngstyleoptiontoolbar.h)
NG_CLASS_LIB(QStyleOptionProgressBar, ngui::NGStyleOptionProgressBar, ngwidgets/ngstyleoptionprogressbar.h)
NG_CLASS_LIB(QStyleOptionMenuItem, ngui::NGStyleOptionMenuItem, ngwidgets/ngstyleoptionmenuitem.h)
NG_CLASS_LIB(QStyleOptionDockWidget, ngui::NGStyleOptionDockWidget, ngwidgets/ngstyleoptiondockwidget.h)
NG_CLASS_LIB(QStyleOptionViewItem, ngui::NGStyleOptionViewItem, ngwidgets/ngstyleoptionviewitem.h)
NG_CLASS_LIB(QStyleOptionToolBox, ngui::NGStyleOptionToolBox, ngwidgets/ngstyleoptiontoolbox.h)
NG_CLASS_LIB(QStyleOptionRubberBand, ngui::NGStyleOptionRubberBand, ngwidgets/ngstyleoptionrubberband.h)
NG_CLASS_LIB(QStyleOptionComplex, ngui::NGStyleOptionComplex, ngwidgets/ngstyleoptioncomplex.h)
NG_CLASS_LIB(QStyleOptionSlider, ngui::NGStyleOptionSlider, ngwidgets/ngstyleoptionslider.h)
NG_CLASS_LIB(QStyleOptionSpinBox, ngui::NGStyleOptionSpinBox, ngwidgets/ngstyleoptionspinbox.h)
NG_CLASS_LIB(QStyleOptionToolButton, ngui::NGStyleOptionToolButton, ngwidgets/ngstyleoptiontoolbutton.h)
NG_CLASS_LIB(QStyleOptionComboBox, ngui::NGStyleOptionComboBox, ngwidgets/ngstyleoptioncombobox.h)
NG_CLASS_LIB(QStyleOptionTitleBar, ngui::NGStyleOptionTitleBar, ngwidgets/ngstyleoptiontitlebar.h)
NG_CLASS_LIB(QStyleOptionGroupBox, ngui::NGStyleOptionGroupBox, ngwidgets/ngstyleoptiongroupbox.h)
NG_CLASS_LIB(QStyleOptionSizeGrip, ngui::NGStyleOptionSizeGrip, ngwidgets/ngstyleoptionsizegrip.h)
NG_CLASS_LIB(QStyleOptionGraphicsItem, ngui::NGStyleOptionGraphicsItem, ngwidgets/ngstyleoptiongraphicsitem.h)
NG_CLASS_LIB(QStyleHintReturn, ngui::NGStyleHintReturn, ngwidgets/ngstylehintreturn.h)
NG_CLASS_LIB(QStyleHintReturnMask, ngui::NGStyleHintReturnMask, ngwidgets/ngstylehintreturnmask.h)
NG_CLASS_LIB(QStyleHintReturnVariant, ngui::NGStyleHintReturnVariant, ngwidgets/ngstylehintreturnvariant.h)
NG_CLASS_LIB(QStyleFactoryInterface, ngui::NGStyleFactoryInterface, ngwidgets/ngstylefactoryinterface.h)
NG_CLASS_LIB(QStylePlugin, ngui::NGStylePlugin, ngwidgets/ngstyleplugin.h)
NG_CLASS_LIB(QTextTable, ngui::NGTextTable, ngwidgets/ngtexttable.h)
NG_CLASS_LIB(QCompleter, ngui::NGCompleter, ngwidgets/ngcompleter.h)
NG_CLASS_LIB(QDesktopServices, ngui::NGDesktopServices, ngwidgets/ngdesktopservices.h)
NG_CLASS_LIB(QScroller, ngui::NGScroller, ngwidgets/ngscroller.h)
NG_CLASS_LIB(QScrollerProperties, ngui::NGScrollerProperties, ngwidgets/ngscrollerproperties.h)
NG_CLASS_LIB(QUndoView, ngui::NGUndoView, ngwidgets/ngundoview.h)
NG_CLASS_LIB(QAbstractButton, ngui::NGAbstractButton, ngwidgets/ngabstractbutton.h)
NG_CLASS_LIB(QAbstractScrollArea, ngui::NGAbstractScrollArea, ngwidgets/ngabstractscrollarea.h)
NG_CLASS_LIB(QAbstractSlider, ngui::NGAbstractSlider, ngwidgets/ngabstractslider.h)
NG_CLASS_LIB(QAbstractSpinBox, ngui::NGAbstractSpinBox, ngwidgets/ngabstractspinbox.h)
NG_CLASS_LIB(QButtonGroup, ngui::NGButtonGroup, ngwidgets/ngbuttongroup.h)
NG_CLASS_LIB(QCalendarWidget, ngui::NGCalendarWidget, ngwidgets/ngcalendarwidget.h)
NG_CLASS_LIB(QCheckBox, ngui::NGCheckBox, ngwidgets/ngcheckbox.h)
NG_CLASS_LIB(QComboBox, ngui::NGComboBox, ngwidgets/ngcombobox.h)
NG_CLASS_LIB(QCommandLinkButton, ngui::NGCommandLinkButton, ngwidgets/ngcommandlinkbutton.h)
NG_CLASS_LIB(QDateTimeEdit, ngui::NGDateTimeEdit, ngwidgets/ngdatetimeedit.h)
NG_CLASS_LIB(QTimeEdit, ngui::NGTimeEdit, ngwidgets/ngtimeedit.h)
NG_CLASS_LIB(QDateEdit, ngui::NGDateEdit, ngwidgets/ngdateedit.h)
NG_CLASS_LIB(QDial, ngui::NGDial, ngwidgets/ngdial.h)
NG_CLASS_LIB(QDialogButtonBox, ngui::NGDialogButtonBox, ngwidgets/ngdialogbuttonbox.h)
NG_CLASS_LIB(QDockWidget, ngui::NGDockWidget, ngwidgets/ngdockwidget.h)
NG_CLASS_LIB(QFocusFrame, ngui::NGFocusFrame, ngwidgets/ngfocusframe.h)
NG_CLASS_LIB(QFontComboBox, ngui::NGFontComboBox, ngwidgets/ngfontcombobox.h)
NG_CLASS_LIB(QFrame, ngui::NGFrame, ngwidgets/ngframe.h)
NG_CLASS_LIB(QGroupBox, ngui::NGGroupBox, ngwidgets/nggroupbox.h)
NG_CLASS_LIB(QKeySequenceEdit, ngui::NGKeySequenceEdit, ngwidgets/ngkeysequenceedit.h)
NG_CLASS_LIB(QLabel, ngui::NGLabel, ngwidgets/nglabel.h)
NG_CLASS_LIB(QLCDNumber, ngui::NGLCDNumber, ngwidgets/nglcdnumber.h)
NG_CLASS_LIB(QLineEdit, ngui::NGLineEdit, ngwidgets/nglineedit.h)
NG_CLASS_LIB(QMainWindow, ngui::NGMainWindow, ngwidgets/ngmainwindow.h)
NG_CLASS_LIB(QMdiArea, ngui::NGMdiArea, ngwidgets/ngmdiarea.h)
NG_CLASS_LIB(QMdiSubWindow, ngui::NGMdiSubWindow, ngwidgets/ngmdisubwindow.h)
NG_CLASS_LIB(QMenu, ngui::NGMenu, ngwidgets/ngmenu.h)
NG_CLASS_LIB(QMenuBar, ngui::NGMenuBar, ngwidgets/ngmenubar.h)
NG_CLASS_LIB(QMenuItem, ngui::NGMenuItem, ngwidgets/ngmenuitem.h)
NG_CLASS_LIB(QPlainTextEdit, ngui::NGPlainTextEdit, ngwidgets/ngplaintextedit.h)
NG_CLASS_LIB(QProgressBar, ngui::NGProgressBar, ngwidgets/ngprogressbar.h)
NG_CLASS_LIB(QPushButton, ngui::NGPushButton, ngwidgets/ngpushbutton.h)
NG_CLASS_LIB(QRadioButton, ngui::NGRadioButton, ngwidgets/ngradiobutton.h)
NG_CLASS_LIB(QRubberBand, ngui::NGRubberBand, ngwidgets/ngrubberband.h)
NG_CLASS_LIB(QScrollArea, ngui::NGScrollArea, ngwidgets/ngscrollarea.h)
NG_CLASS_LIB(QScrollBar, ngui::NGScrollBar, ngwidgets/ngscrollbar.h)
NG_CLASS_LIB(QSizeGrip, ngui::NGSizeGrip, ngwidgets/ngsizegrip.h)
NG_CLASS_LIB(QSlider, ngui::NGSlider, ngwidgets/ngslider.h)
NG_CLASS_LIB(QSpinBox, ngui::NGSpinBox, ngwidgets/ngspinbox.h)
NG_CLASS_LIB(QDoubleSpinBox, ngui::NGDoubleSpinBox, ngwidgets/ngdoublespinbox.h)
NG_CLASS_LIB(QSplashScreen, ngui::NGSplashScreen, ngwidgets/ngsplashscreen.h)
// NG_CLASS_LIB(QSplitter, ngui::NGSplitter, ngwidgets/ngsplitter.h)
NG_CLASS_LIB(QSplitterHandle, ngui::NGSplitterHandle, ngwidgets/ngsplitterhandle.h)
NG_CLASS_LIB(QStackedWidget, ngui::NGStackedWidget, ngwidgets/ngstackedwidget.h)
// NG_CLASS_LIB(QStatusBar, ngui::NGStatusBar, ngwidgets/ngstatusbar.h)
NG_CLASS_LIB(QTabBar, ngui::NGTabBar, ngwidgets/ngtabbar.h)
NG_CLASS_LIB(QTabWidget, ngui::NGTabWidget, ngwidgets/ngtabwidget.h)
NG_CLASS_LIB(QTextBrowser, ngui::NGTextBrowser, ngwidgets/ngtextbrowser.h)
NG_CLASS_LIB(QTextEdit, ngui::NGTextEdit, ngwidgets/ngtextedit.h)
NG_CLASS_LIB(QToolBar, ngui::NGToolBar, ngwidgets/ngtoolbar.h)
NG_CLASS_LIB(QToolBox, ngui::NGToolBox, ngwidgets/ngtoolbox.h)
NG_CLASS_LIB(QToolButton, ngui::NGToolButton, ngwidgets/ngtoolbutton.h)